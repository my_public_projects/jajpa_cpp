#run in project root dir
#Specify our own paths 
LIB_STD_17=/usr/local/lib64
CXX_COMPILER=/usr/local/bin/g++
C_COMPILER=/usr/local/bin/gcc


export LD_LIBRARY_PATH=${LIB_STD_17}:${LD_LIBRARY_PATH}
rm -r build_tests
mkdir build_tests
cd build_tests

cmake ../jajpa_cpp/ -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=${CXX_COMPILER} -DCMAKE_C_COMPILER=${C_COMPILER}
cmake --build .

./jsonparse_test
